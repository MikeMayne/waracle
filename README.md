# c++ code test v1.1 #

This code test was written by Michael Mayne mikemayne@gmail.com, built and tested on Ubuntu 16.04.

## build library ##

mkdir lib
cd build
make

## testing setup ##

you'll need to clone googletest to run the unit tests. From project root:

cd test && git clone https://github.com/google/googletest.git

## run unit tests ##

From project root:

cd test
make && ./analyse_unittest
