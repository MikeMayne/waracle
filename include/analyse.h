/**
 * @author Michael Mayne mikemayne@gmail.com
 */

extern "C" 
{
	/**
	 * @brief 
	 * 
	 * generates the count, sum and average of a list of integers
	 * 
	 * @details 
	 * 
	 * Given a path to an input file containing a list of whole numbers.
	 * 	
	 * The function writes the folling analysis to an output file:
	 *	- Writes the count of the numbers read to a line
	 *	- Write the sum of the numbers read to a line
	 *	- Write the average of the numbers read to a line
	 * 
	 * @param input  the path to the file to read from
	 * @param output the path to the file to write to
	 * 
	 * @return 
	 * 
	 * Returns 0 if the operation was successful, or a negative number otherwise
	 */
	int analyse(const char * input, const char * output);
}
