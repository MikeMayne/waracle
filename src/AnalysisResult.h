/**
 * @author Michael Mayne mikemayne@gmail.com
 */

#include <iostream>
#include <fstream>

/**
 * @brief 
 * 
 * A immutable object that can be built from a 
 * text file or stream, and written to a text file
 * or stream.
 * 
 */
class AnalysisResult
{
public:
	/**
	 * @brief 
	 * 
	 * Default zero result object
	 * 
	 * @details
	 * 
	 * so you can put it in a collection.
	 * 
	 */
	AnalysisResult();
	virtual ~AnalysisResult() = default;

	/**
	 * @brief
	 * 
	 * opens file at inputPath, returns an AnalysisResult
	 * 
	 * @details
	 * 
	 * throws a std::runtime_error if the file cannot be opened, or contains 
	 * data that cannot be processed
	 * 
	 * @param inputPath 
	 * 
	 * path to the input file
	 * 
	 * @return
	 * 
	 * AnalysisResult 
	 * 
	 * @throw
	 * 
	 * std::runtime_error
	 * 
	 */
	static AnalysisResult fromFile(const char * inputPath);

	/**
	 * @brief 
	 * processes an input stream, returns an AnalysisResult.
	 * 
	 * @details
	 * may throw if the stream throws an exception, or
	 * contains data that cannot be processed
	 * 
	 * @param inputStream an inputStream to be read from
	 * 
	 * @return
	 * 
	 * AnalysisResult
	 * 
	 * @throw
	 * 
	 * std::runtime_error
	 */
	static AnalysisResult fromInputStream(std::istream &inputStream);

	// the class is immutable, it could be extended to add setters.
	int getCount();
	int getSum();
	double getAverage();

	/**
	 * @brief
	 * 
	 * writes the AnalysisResult to a file at the given path.
	 * 
	 * @param outputPath 
	 * @return
	 * 
	 * AnalysisResult::Success on sucessful write
	 * AnalysisResult::failure if the write is not successful
	 */
	int write(const char* outputPath);	

	/**
	 * @brief
	 * 
	 * writes the AnalysisResult to an output stream.
	 * 
	 * @details 
	 * 
	 * @param outputPath 
	 * 
	 * @return
	 * 
	 * AnalysisResult::Success on sucessful write
	 * AnalysisResult::failure if the write is not successful
	 */
	int write(std::ostream &outputStream);

	// Error codes.  More codes could be added if more detail
	// is needed about the causes of errors.
	static const int Success;
	static const int Failure;

protected:
	/**
	 * @brief 
	 * 
	 * called by fromInputStream to process the input stream.
	 * 
	 * @details 
	 * 
	 * override in subclasses to extend functionality
	 * 
	 * @param input 
	 * 
	 * the input stream to be read from
	 * 
	 * @return 
	 * 
	 * AnalysisResult::Success
	 * AnalysisResult::Failure
	 * 
	 */
	virtual int process(std::istream &input);

	/**
	 * @brief
	 * 
	 * process a single line from the input file
	 *
	 * @details override to add functionality
	 * 
	 * eg. ignore lines that start with //
	 * eg. extra processing min, max, median
	 * 
	 * @param  value
	 */
	virtual void processLine(std::string value);

	/**
	 * @brief
	 * 
	 * called once after all the values have been processed
	 * individually to calculate the average
	 *
	 * @details override to add extra processing
	 * 
	 */
	virtual void postProcess();
private:
	int count;
	int sum;
	double average;
};
