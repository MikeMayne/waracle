/**
 * @author Michael Mayne mikemayne@gmail.com
 */

#include <analyse.h>
#include "AnalysisResult.h"

const int AnalysisResult::Success = 0;
const int AnalysisResult::Failure = -1;

AnalysisResult::AnalysisResult()
: count (0)
, sum (0)
, average (0.)
{}

AnalysisResult AnalysisResult::fromFile(const char * inputPath)
{
	std::ifstream input;
	input.open(inputPath);

	return AnalysisResult::fromInputStream (input);
}

AnalysisResult AnalysisResult::fromInputStream(std::istream &inputStream)
{
	AnalysisResult result;

	if (result.process(inputStream) == Failure) // don't construct an invalid object
		throw std::runtime_error("Error processing input file.");

	return result;
}

int AnalysisResult::getCount()
{
	return count;
}

int AnalysisResult::getSum()
{
	return sum;
}

double AnalysisResult::getAverage()
{
	return average;
}

int AnalysisResult::process(std::istream &input)
{
	try 
	{
		std::string line;

		while (std::getline(input, line))
		{
			processLine(line);
		}

		postProcess();
	}
	catch (std::exception)
	{
		return AnalysisResult::Failure;
	}

	return AnalysisResult::Success;
}

void AnalysisResult::processLine(std::string line)
{
	int value = std::stoi(line);
	sum += value;
	count++;
}

void AnalysisResult::postProcess()
{
	if (count > 0)
		average = sum / static_cast <double> (count);
}

int AnalysisResult::write(const char* outputPath)
{
	try
	{
		std::ofstream outputStream;
		outputStream.open(outputPath);
		return write (outputStream);
	}
	catch (std::exception)
	{
		return AnalysisResult::Failure;
	}

	return AnalysisResult::Success;
}


int AnalysisResult::write(std::ostream &outputStream)
{
	try
	{
		outputStream << std::to_string(count) << std::endl;
		outputStream << std::to_string(sum) << std::endl;
		outputStream << std::to_string(average) << std::endl;
	}
	catch (std::exception)
	{
		return AnalysisResult::Failure;
	}

	if (outputStream.bad() || outputStream.fail())
		return AnalysisResult::Failure;

	return AnalysisResult::Success;
}

int analyse(const char * input, const char * output)
{
	try 
	{
		auto result = AnalysisResult::fromFile(input);
		return result.write(output);
	} 
	catch (std::exception)
	{
		return AnalysisResult::Failure;
	}
}
