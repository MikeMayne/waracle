#include <gtest/gtest.h>
#include "../include/analyse.h"
#include "../src/AnalysisResult.h"

namespace {

TEST(TestAnalysisResult, ProcessWorks) 
{
	std::stringstream input;
	input << "10" << std::endl;
	input << "10" << std::endl;
	input << "10" << std::endl;

	auto result = AnalysisResult::fromInputStream(input);

	EXPECT_EQ(result.getCount(), 3);
	EXPECT_EQ(result.getSum(), 30);
	EXPECT_EQ(result.getAverage(), 10.0);
}

TEST(TestAnalysisResult, ProcessWorksWithEmptyInput) 
{
	std::stringstream input;
	auto result = AnalysisResult::fromInputStream(input);

	EXPECT_EQ(result.getCount(), 0);
	EXPECT_EQ(result.getSum(), 0);
	EXPECT_EQ(result.getAverage(), 0.);
}

TEST(TestAnalysisResult, ProcessWorksWithSingleInput) 
{
	std::stringstream input;
	input << "10" << std::endl;

	auto result = AnalysisResult::fromInputStream(input);

	EXPECT_EQ(result.getCount(), 1);
	EXPECT_EQ(result.getSum(), 10);
	EXPECT_EQ(result.getAverage(), 10.);
}

TEST(TestAnalysisResult, ProcessThrowsWithBadData) 
{
	std::stringstream input;
	input << "10" << std::endl;
	input << "a" << std::endl;

	EXPECT_THROW({
		auto result = AnalysisResult::fromInputStream(input);
		(void)result; // avoid -Wunused-but-set-variable
	}, std::exception);
}

TEST(TestAnalyse, ReturnsZeroWhenInputFileEmpty) 
{
	EXPECT_EQ(AnalysisResult::Success, analyse("no-input.txt", "zero-output.txt"));
}

TEST(TestAnalyse, ReturnsZeroWithGoodInput) 
{
	EXPECT_EQ(AnalysisResult::Success, analyse("good-input.txt", "good-output.txt"));
}

TEST(TestAnalyse, ReturnsNegativeWithBadInputFile) 
{
	EXPECT_EQ(AnalysisResult::Failure, analyse("bad-input.txt", "zero-output2.txt"));
}

TEST(TestAnalyse, ReturnsNegativeWithEmptyPaths) 
{
	EXPECT_EQ(AnalysisResult::Failure, analyse("", ""));
}

} //namespace